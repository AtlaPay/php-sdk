<?php

require 'vendor/autoload.php';

use AtlaPay\AtlaPayClient;
use AtlaPay\AtlaPayClientModes;
use AtlaPay\TokenService;

define('API_KEY', '222222');

$atlaPayClient = new AtlaPayClient(API_KEY, AtlaPayClientModes::SANDBOX);
// Enable for local development
$atlaPayClient->setApiHost('localhost:3000');
$atlaPayClient->setApiIsHttps(false);
$tokenService = new TokenService($atlaPayClient);

// -----------------------------------------------------------------------------------
// Tokenization Example
// -----------------------------------------------------------------------------------

// An string can be tokenized
// $token = $tokenService->tokenize('hello');
$importantData = [
    'pan' => '4100123412341234',
    'expirationDate' => '12/22',
    'cardHolder' => 'John Doe',
];
$result = $tokenService->tokenize($importantData);

echo 'Tokenization Result' . PHP_EOL;
print_r($result);

// -----------------------------------------------------------------------------------
// Detokenization Example
// -----------------------------------------------------------------------------------

$token = $result->token;
$result = $tokenService->detokenize($token);

echo 'Detokenization Result' . PHP_EOL;
print_r($result);

// -----------------------------------------------------------------------------------
// Validate Token Example
// -----------------------------------------------------------------------------------

$result = $tokenService->validate($token);

echo 'Validation Result' . PHP_EOL;
print_r($result);

// -----------------------------------------------------------------------------------
// Delete Token Example
// -----------------------------------------------------------------------------------

$result = $tokenService->delete($token);

echo 'Delete Token Result' . PHP_EOL;
print_r($result);