<?php

namespace AtlaPay;

abstract class AtlaPayClientModes
{
    const LIVE = 'live';
    const SANDBOX = 'sandbox';
}